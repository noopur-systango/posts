export const columns = [
          {
            Header: 'Id',  
            accessor:'id',
            filterable:false,
            show: true,
            headerClassName: 'text-info text-uppercase',
            width:150    
          },  
          {
            Header: 'Name',  
            accessor: 'name',
            filterable:false,
            show: true, 
            headerClassName: 'text-info text-uppercase',
            width:200
          },    
          {
            Header: 'Price',  
            accessor: 'price', 
            filterable:false,
            show: true,   
            headerClassName: 'text-info text-uppercase',
            width:150
          },
          {
            Header: 'Color',  
            accessor: 'color', 
            filterable:false,
            show: true,
            headerClassName: 'text-info text-uppercase', 
            width:150      
          },
          {
            Header: 'Availability',  
            accessor: 'availability', 
            filterable:false,
            show: true,   
            headerClassName: 'text-info text-uppercase',
            width:200
          },
          {
            Header: 'Dimension',  
            accessor: 'dimension', 
            filterable:false,
            show: true,
            headerClassName: 'text-info text-uppercase', 
            width:150      
          },
        ];
        
export const defaultPageSize = 10;        