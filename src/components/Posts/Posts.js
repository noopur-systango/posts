import React, { Component } from "react";
import axios from "axios";
import ReactTable from "react-table-6";
import { Button, ButtonGroup } from "react-bootstrap";
import "react-table-6/react-table.css";
import Modal from "../UI/Modal";
import Navbar from "../UI/Navbar";
import Footer from "../UI/Footer";
import { GET_JSON } from "../../constants/APIurl";
import { columns, defaultPageSize } from "../../constants/Constants";
import "./Posts.css";

class Data extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      isModalOpen: false,
      selectedRow: null,
      pages: 1,
      searchInput: "",
      filteredData: null,
    };
  }

  componentDidMount() {
    let { pages } = this.state;
    axios.get(`${GET_JSON}?limit=10&page=${pages}`).then((response) => {
      this.setState({ posts: response.data, pages: pages });
    });
  }

  handleInput = (event) => {
    this.setState({ searchInput: event.target.value }, () => {
      this.globalSearch();
    });
  };

  globalSearch = () => {
    let { searchInput } = this.state;
    let filteredData = this.state.posts.filter((value) => {
      return (
        value.name.toLowerCase().includes(searchInput.toLowerCase()) ||
        value.color.toLowerCase().includes(searchInput.toLowerCase()) ||
        value.availability.toLowerCase().includes(searchInput.toLowerCase()) ||
        value.dimension.toLowerCase().includes(searchInput.toLowerCase()) ||
        value.price
          .toString()
          .toLowerCase()
          .includes(searchInput.toLowerCase()) ||
        value.id.toString().toLowerCase().includes(searchInput.toLowerCase())
      );
    });
    if (searchInput === " ") {
      this.setState({ filteredData: null });
    }
    this.setState({ filteredData: filteredData });
  };

  onRowClick = (data, props) => {
    return {
      onClick: () => {
        this.setState({ isModalOpen: true, selectedRow: props.original });
      },
    };
  };

  closeModal = () => {
    this.setState({ isModalOpen: false });
  };

  showRow = (data) => {
    this.setState({ isModalOpen: true, selectedRow: data });
  };

  getPage = (event) => {
    const { pages } = this.state;
    const id = event.target.id;
    const updatedPageNo = id === "prev" ? pages - 1 : pages + 1;
    axios.get(`${GET_JSON}?limit=10&page=${updatedPageNo}`).then((response) => {
      this.setState({
        posts: response.data,
        pages: updatedPageNo,
        filteredData: null,
        searchInput: "",
      });
    });
  };

  render() {
    const { posts, isModalOpen, selectedRow, filteredData } = this.state;
    return (
      <div className="App">
        <Navbar
          handleInput={this.handleInput}
          searchInput={this.state.searchInput}
        />
        <div className="container h-100">
          <h3 className="text-info font-weight-bold">Posts Table Data</h3>
          <ReactTable
            loading={!this.state.posts.length}
            data={filteredData ? filteredData : posts}
            columns={columns}
            defaultPageSize={defaultPageSize}
            getTrProps={this.onRowClick}
            showPaginationBottom={true}
            pages={defaultPageSize}
            showPagination={false}
            className="-striped -highlight"
          />
          <Modal
            isOpen={isModalOpen}
            showRow={this.onRowClick}
            selectedRow={selectedRow}
            closeModal={this.closeModal}
          />
        </div>
        <br />
        <div className="d-inline-flex">
          <ButtonGroup className="mr-2" aria-label="First group">
            <Button
              variant="info"
              id="prev"
              type="prev"
              onClick={this.getPage}
              disabled={this.state.pages <= 1}
            >
              Previous
            </Button>
            <Button variant="light">
              {this.state.pages === "0" ? "1" : this.state.pages + " / 5"}
            </Button>{" "}
            <Button
              variant="info"
              id="next"
              type="next"
              onClick={this.getPage}
              disabled={this.state.pages >= 5}
            >
              Next
            </Button>
          </ButtonGroup>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Data;
