import React, { Component } from 'react';
import { Navbar } from 'react-bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';

class DataFooter extends Component {
  render(){
    return(
      <Navbar bg="light" expand="lg" className="justify-content-center">
      <Navbar aria-controls="basic-navbar-nav" /> 
      <Navbar.Text className="text-info justify-content-center">
        <img
          src={require('../../assests/logo.png')}
          width="30"
          height="30"
          className="d-inline-block align-bottom"
          alt="Footer logo"/> 
          Made with Love from : <a href="https://www.systango.com/">Systango</a>
      </Navbar.Text>
      </Navbar>
      )
    }
  }

  export default DataFooter;