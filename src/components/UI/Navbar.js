import React, { Component } from 'react';
import { Navbar, Form,  Nav, FormControl} from 'react-bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';

class DataNavbar extends Component {
  render(){
    return(
      <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home" className="text-info">
        <img
          src={require('../../assests/logo.png')}
          width="40"
          height="40"
          className="d-inline-block align-top"
          alt="Posts"
        />
      <strong> POSTS </strong>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto"></Nav>
      <Form inline>
        <FormControl 
            type="text" 
            value={this.props.searchInput}
            placeholder="Search" 
            name="searchInput" 
            onChange={this.props.handleInput}
            className="mr-sm-2" 
            />
      </Form>
      </Navbar.Collapse>
      </Navbar>
      )
    }
  }

export default DataNavbar;