import React, { Component } from 'react';
import { Modal , Button } from 'react-bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';

class DataModal extends Component {
  render(){
    return(
      <Modal show={this.props.isOpen} onHide={this.props.closeModal}>
      <Modal.Header closeButton>
        <Modal.Title className="text-info"><strong> Post Detail </strong>
          {this.props.selectedRow && <span className="text-info rounded-circle p-2 border border-info">{this.props.selectedRow.id}</span>}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {this.props.selectedRow && <span className="text-dark font-weight-bold"><strong>Name : </strong>
          {this.props.selectedRow.name}</span>}<br/>
        {this.props.selectedRow && <span className="text-info"><strong>Price : </strong>
          {this.props.selectedRow.price}</span>}<br/> 
        {this.props.selectedRow && <span className="text-info"><strong>Color : </strong>
          {this.props.selectedRow.color}</span>}<br/> 
        {this.props.selectedRow && <span className="text-info"><strong>Availability : </strong>
          {this.props.selectedRow.availability}</span>}<br/> 
        {this.props.selectedRow && <span className="text-info"><strong>Dimension : </strong>
          {this.props.selectedRow.dimension}</span>}<br/>       
      </Modal.Body>
      <Modal.Footer>
          <Button variant="info" onClick={this.props.closeModal}>Close</Button>
      </Modal.Footer>
      </Modal>
      )
    }
  }

export default DataModal;